from PyQt5 import QtCore, QtGui, QtWidgets
import math


class Ui_MainWindow(object):
    global q
    q = 1
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(439, 314)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.btn_start = QtWidgets.QPushButton(self.centralwidget)
        self.btn_start.setGeometry(QtCore.QRect(10, 280, 80, 25))
        self.btn_start.setObjectName("btn_start")
        self.btn_1 = QtWidgets.QRadioButton(self.centralwidget)
        self.btn_1.setGeometry(QtCore.QRect(10, 30, 211, 23))
        self.btn_1.setObjectName("btn_1")
        self.btn_2 = QtWidgets.QRadioButton(self.centralwidget)
        self.btn_2.setGeometry(QtCore.QRect(10, 50, 211, 23))
        self.btn_2.setObjectName("btn_2")
        self.btn_3 = QtWidgets.QRadioButton(self.centralwidget)
        self.btn_3.setGeometry(QtCore.QRect(10, 70, 211, 23))
        self.btn_3.setObjectName("btn_3")
        self.btn_4 = QtWidgets.QRadioButton(self.centralwidget)
        self.btn_4.setGeometry(QtCore.QRect(10, 90, 211, 23))
        self.btn_4.setObjectName("btn_4")
        self.btn_5 = QtWidgets.QRadioButton(self.centralwidget)
        self.btn_5.setGeometry(QtCore.QRect(10, 110, 211, 23))
        self.btn_5.setObjectName("btn_5")
        self.btn_6 = QtWidgets.QRadioButton(self.centralwidget)
        self.btn_6.setGeometry(QtCore.QRect(10, 130, 211, 23))
        self.btn_6.setObjectName("btn_6")
        self.btn_12 = QtWidgets.QRadioButton(self.centralwidget)
        self.btn_12.setGeometry(QtCore.QRect(10, 250, 211, 23))
        self.btn_12.setObjectName("btn_12")
        self.btn_11 = QtWidgets.QRadioButton(self.centralwidget)
        self.btn_11.setGeometry(QtCore.QRect(10, 230, 211, 23))
        self.btn_11.setObjectName("btn_11")
        self.btn_10 = QtWidgets.QRadioButton(self.centralwidget)
        self.btn_10.setGeometry(QtCore.QRect(10, 210, 211, 23))
        self.btn_10.setObjectName("btn_10")
        self.btn_9 = QtWidgets.QRadioButton(self.centralwidget)
        self.btn_9.setGeometry(QtCore.QRect(10, 190, 211, 23))
        self.btn_9.setObjectName("btn_9")
        self.btn_8 = QtWidgets.QRadioButton(self.centralwidget)
        self.btn_8.setGeometry(QtCore.QRect(10, 170, 211, 23))
        self.btn_8.setObjectName("btn_8")
        self.btn_7 = QtWidgets.QRadioButton(self.centralwidget)
        self.btn_7.setGeometry(QtCore.QRect(10, 150, 211, 23))
        self.btn_7.setObjectName("btn_7")
        self.result_label = QtWidgets.QLabel(self.centralwidget)
        self.result_label.setGeometry(QtCore.QRect(220, 30, 201, 251))
        self.result_label.setObjectName("result_label")
        self.result_label.setWordWrap(True)
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(210, 20, 221, 271))
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.calc_by = QtWidgets.QLabel(self.centralwidget)
        self.calc_by.setGeometry(QtCore.QRect(10, 10, 181, 21))
        self.calc_by.setObjectName("calc_by")
        self.frame.raise_()
        self.btn_start.raise_()
        self.btn_1.raise_()
        self.btn_2.raise_()
        self.btn_3.raise_()
        self.btn_4.raise_()
        self.btn_5.raise_()
        self.btn_6.raise_()
        self.btn_12.raise_()
        self.btn_11.raise_()
        self.btn_10.raise_()
        self.btn_9.raise_()
        self.btn_8.raise_()
        self.btn_7.raise_()
        self.result_label.raise_()
        self.calc_by.raise_()
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Расчет элементов Хорда – Прогиб – Радиус"))
        self.btn_start.setText(_translate("MainWindow", "Start"))
        self.btn_1.setText(_translate("MainWindow", "Хорде – Радиусу"))
        self.btn_2.setText(_translate("MainWindow", "Хорде и Углу раствора"))
        self.btn_3.setText(_translate("MainWindow", "координат Радиусной кривой"))
        self.btn_4.setText(_translate("MainWindow", "Хорде и длине Дуги"))
        self.btn_5.setText(_translate("MainWindow", "Хорде и Прогибу"))
        self.btn_6.setText(_translate("MainWindow", "Радиусу и Углу раствора"))
        self.btn_12.setText(_translate("MainWindow", "Правильного многогранника"))
        self.btn_11.setText(_translate("MainWindow", "Углу раствора и длине Дуги"))
        self.btn_10.setText(_translate("MainWindow", "Прогибу и Углу раствора"))
        self.btn_9.setText(_translate("MainWindow", "Прогибу и длине Дуги"))
        self.btn_8.setText(_translate("MainWindow", "Радиусу и длине Дуги"))
        self.btn_7.setText(_translate("MainWindow", "Радиусу и Прогибу"))
        self.result_label.setText(_translate("MainWindow", "Здесь будут ваши результаты!"))
        self.calc_by.setText(_translate("MainWindow", "Расчёт по.../Расчёт..."))
        self.btn_1.setChecked(True)
        self.btn_start.clicked.connect(self.start)
        self.btn_1.clicked.connect(self.check1)
        self.btn_2.clicked.connect(self.check2)
        self.btn_3.clicked.connect(self.check3)
        self.btn_4.clicked.connect(self.check4)
        self.btn_5.clicked.connect(self.check5)
        self.btn_6.clicked.connect(self.check6)
        self.btn_7.clicked.connect(self.check7)
        self.btn_8.clicked.connect(self.check8)
        self.btn_9.clicked.connect(self.check9)
        self.btn_10.clicked.connect(self.check10)
        self.btn_11.clicked.connect(self.check11)
        self.btn_12.clicked.connect(self.check12)

    def check1(self):
        global q
        if self.btn_1.isChecked(): 
            q = 1
        else:
            pass
    def check2(self):
        global q
        if self.btn_2.isChecked(): 
            q = 2
        else:
            pass    
    def check3(self):
        global q
        if self.btn_3.isChecked(): 
            q = 3
        else:
            pass
    def check4(self):
        global q
        if self.btn_4.isChecked(): 
            q = 4
        else:
            pass
    def check5(self):
        global q
        if self.btn_5.isChecked(): 
            q = 5
        else:
            pass
    def check6(self):
        global q
        if self.btn_6.isChecked(): 
            q = 6
        else:
            pass
    def check7(self):
        global q
        if self.btn_7.isChecked(): 
            q = 7
        else:
            pass
    def check8(self):
        global q
        if self.btn_8.isChecked(): 
            q = 8
        else:
            pass
    def check9(self):
        global q
        if self.btn_9.isChecked(): 
            q = 9
        else:
            pass
    def check10(self):
        global q
        if self.btn_10.isChecked(): 
            q = 10
        else:
            pass
    def check11(self):
        global q
        if self.btn_11.isChecked(): 
            q = 11
        else:
            pass
    def check12(self):
        global q
        if self.btn_12.isChecked(): 
            q = 12
        else:
            pass

    def start(self):
        global q
        global Rad # Радиус – Обьявили глобальную переменную
        global Diam # Диаметр
        global Xord # Хорда
        global Progi # Прогиб
        global UgSe # Угол раствора радиусов на хорду
        global Dug # Длина дуги над хордой
        global PlSe # Площадь сектора на хорду
        global PlTr # Площадь Треугольника под хордой
        global PlGo # Площадь Горбушки
        try:
            Pii=0.000000
            Pii=math.pi # Вытащили число " Пи "
            Kvz=0 # Флаг – Если =1 то работа уже выполнена.
            if q==1: # Расчет по Хорде – Радиусу
                a=0.00000000
                a, done1 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите Хорду: ')
                a=float(a) # Принудительно в вещественное число
                R=0.00000000
                R, done2 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Радиус: ')
                R=float(R) # Принудительно в вещественное число
                x=0.00000000
                y=0.00000000
                v=0.00000000
                Sk=0.0000000
                St=0.00000000
                S=0.000000000
                x=((R*R)-(a*a/4))
                b=R-(math.sqrt(x)) # Квадратный корень из " x "
                x=(a/2)/R
                y=math.asin(x)
                v=math.cos(y)
                aur=2*y
                au=aur*180/Pii # Угол А в градусах
                Kvz=1 # Коэф – Расчет уже выполнен
            # Далее Конец Cдвига – четыре пробела в начале каждой строки
            else:
            # Далее Cдвиг – четыре пробела в начале каждой строки
                Kvz=0
            # ...... ....... ....... ....... ....... ........ .......
            # Далее Конец Cдвига – четыре пробела в начале каждой строки
            if q==2: # Расчет по Хорде и углу раствора
                # Далее Cдвиг – четыре пробела в начале каждой строки
                a=0.00000000
                a, done1 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите Хорду: ')
                a=float(a) # Принудительно в вещественное число
                au=0.00000000
                au, done2 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите Угол раствора в градусах: ')
                au=float(au) # Принудительно в вещественное число
                x=0.00000000
                aur=0.00000000
                v=0.00000000
                y=0.00000000
                Sk=0.0000000
                St=0.00000000
                S=0.000000000
                aur=au*Pii/180
                x=math.sin(aur/2)
                R=(a/2)/x # Нашли радиус
                y=(R*R)-(a*a/4)
                v=math.sqrt(y) # Квадратный корень из " y "
                b=R-v # Нашли прогиб
                Kvz=1 # Коэф – Расчет уже выполнен
            # Далее Конец Cдвига – четыре пробела в начале каждой строки
            else:
            # Далее Cдвиг – четыре пробела в начале каждой строки
                Kvz=0
            # ...... ....... ....... ....... ....... ........ .......
            # Далее Конец Cдвига – четыре пробела в начале каждой строки
            if q<3: # Вывод по Хорде Прогиб Радиус
                # Далее Cдвиг – четыре пробела в начале каждой строки
                D=R+R
                Sk=Pii*D*D*au/(4*360) # Площадь сектора круга с углом aur
                St=(a/2)*(R-b) # Площадь треугольника в секторе
                S = Sk-St # Площадь горбушки
                L=Pii*D*au/360 # Длина дуги
                ss=str(a) # Преобразуем число в строку
                self.result_label.setText(f'Хорда = {str(a)} \nСтрела прогиба = {str(b)}\nРадиус = {str(R)}\nДиаметр = {str(D)}\nУгол раствора хорды = {str(au)}\nДлина дуги над хордой = {str(L)}\nПлощадь сектора = {str(Sk)}\nПлощадь треугольника под горбушкой = {str(St)}\nПлощадь горбушки = {str(S)}')
            # Далее Конец Cдвига – четыре пробела в начале каждой строки
            # ...... ....... ....... ....... ....... ........ .......
            if q==3: # Координаты радиусной прямой
            # Далее Cдвиг – четыре пробела в начале каждой строки
                q=13 # Переброс в конец программы
            # ....... ....... ....... ...... ...... ....... ....... ......
            # Далее Конец Cдвига – четыре пробела в начале каждой строки
            if q==4: # Расчет по Хорде и длине Дуги
                # Далее Cдвиг – четыре пробела в начале каждой строки
                a=0.00000000
                a, done = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите Хорду: ')
                a=float(a) # Принудительно в вещественное число
                L=0.00000000
                R, done2 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите длину Дуги: ')
                L=float(L) # Принудительно в вещественное число
                R=0.00000000
                dx=a/200000
                rt=(a/2)+dx # Начальный радиус расчета
                aa=a/2 # Половина хорды
                dl=Pii*rt
                while dl>L: # Расчет по Хорде и длине Дуги
                    # Далее Cдвиг – восемь пробелов в начале каждой строки
                    rt=rt+dx # Текущий радиус
                    x=aa/rt
                    y=2*(math.asin(x)) # Угол А в радианах через арс синус
                    dl=rt*y # Текущая длина дуги
                # Далее Cдвиг – четыре пробела в начале каждой строки
                yg=y*180/Pii # Угол " y " в градусах
                au=yg
                R=rt # Нашли радиус
                y=(R*R)-(aa*aa)
                v=math.sqrt(y) # Квадратный корень из " y "
                b=R-v # Нашли прогиб
                D=R+R
                Sk=Pii*D*D*yg/(4*360) # Площадь сектора круга с углом aur
                St=aa*v # Площадь треугольника в секторе
                S = Sk-St # Площадь горбушки
            # Далее Конец Cдвига – четыре пробела в начале каждой строки
            else:
            # Далее Cдвиг – четыре пробела в начале каждой строки
                Kvz=0
            # Далее Конец Cдвига – четыре пробела в начале каждой строки
            # ...... ...... ....... ...... ...... ....... ....... ....... ........ ........ ........ .......
            if q==5: # Расчет по Хорде – Прогибу
                # Далее Cдвиг – четыре пробела в начале каждой строки
                a=0.00000000
                a, done1 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите Хорду: ')
                a=float(a) # Хорда – Принудительно в вещественное число
                b=0.00000000
                b, done2 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите Стрелу Прогиба: ')
                b=float(b) # Стрела прогиба – Принудительно в вещественное число
                x=0.00000000
                y=0.00000000
                v=0.00000000
                R=0.00000000
                Sk=0.0000000
                St=0.00000000
                S=0.000000000
                Ex=1.000000
                k=a/2
                Rt=k+(k/100000)
                Rd= k/100000
                t=k/100000000
                # Уточнение подбором
                while Ex >t:
                    # Далее Cдвиг – восемь пробелов в начале каждой строки
                    Rt=Rt+Rd
                    x=((Rt*Rt)-(k*k))
                    c=math.sqrt(x) # Квадратный корень из " x "
                    Ex=(Rt-c)-b
            # E=math.abs(x)
            # Конец подбора ........ ....... ......
                # Далее Cдвиг – четыре пробела в начале каждой строки
                R=Rt
                D=R+R
                x=k/Rt
                sur=math.asin(x) # Угол А в радианах
                su=sur*180/Pii # Угол А в градусах
                au=2*su
                yg=au
                L=(R+R)*Pii*au/360
            # ....... ....... ....... ...... ...... ....... ....... ....... ........
            # Далее Конец Cдвига – четыре пробела в начале каждой строки
            if q==6: # Расчет по Радиусу и Углу раствора
                # Далее Cдвиг – четыре пробела в начале каждой строки
                R=0.00000000
                R, done1 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите Радиус: ')
                R=float(R) # Радиус – Принудительно в вещественное число
                yg=0.00000000
                yg, done2 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите Угол раствора в градусах: ')
                yg=float(yg) # Угол раствора – Принудительно в вещественное число
                yr=yg*Pii/180 # Угол раствора В радианах
                x=0.00000000
                y=0.00000000
                v=0.00000000
                Sk=0.0000000
                St=0.00000000
                S=0.000000000
                Ex=1.000000
                x= math.cos(yr/2)
                y=R*x
                b=R-y
                x=(R*R)-(y*y)
                c=math.sqrt(x) # Квадратный корень из " x "
                a=c+c
                au=yg
                D=R+R
            # .......... ........ ....... ....... ........ ..........
            # Далее Конец Cдвига – четыре пробела в начале каждой строки
            # ....... ....... ....... ...... ...... ....... ....... ....... .
            if q==6: # Расчет по Радиусу и Углу раствора
            # Далее Cдвиг – четыре пробела в начале каждой строки
                R=0.00000000
                R, done1 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите Радиус: ')
                R=float(R) # Радиус – Принудительно в вещественное число
                yg=0.00000000
                yg, done2 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите Угол раствора в градусах: ')
                yg=float(yg) # Угол раствора – Принудительно в вещественное число
                yr=yg*Pii/180 # Угол раствора В радианах
                x=0.00000000
                y=0.00000000
                v=0.00000000
                Sk=0.0000000
                St=0.00000000
                S=0.000000000
                Ex=1.000000
                x= math.cos(yr/2)
                y=R*x
                b=R-y
                x=(R*R)-(y*y)
                c=math.sqrt(x) # Квадратный корень из " x "
                a=c+c
                au=yg
                D=R+R
            # .......... ........ ....... ....... ........ ..........
            # Далее Конец Cдвига – четыре пробела в начале каждой строки
            # ....... ....... ....... ...... ...... ....... ....... ....... .
            if q==7: # Расчет по Радиусу и Прогибу
            # Далее Cдвиг – четыре пробела в начале каждой строки
                R=0.00000000
                R, done1 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите Радиус: ')
                R=float(R) # Радиус – Принудительно в вещественное число
                b=0.00000000
                b, don2 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите Прогиб: ')
                b=float(b) # Угол раствора – Принудительно в вещественное число
                k=R-b
                x=(R*R)-(k*k)
                a=(math.sqrt(x))*2
                x=(a/2)/k
                aur = (math.atan(x))*2
                au=aur*180/Pii
                yg = au
            # ....... ....... ....... ...... ...... ....... ....... ....... ........
            # Далее Конец Cдвига – четыре пробела в начале каждой строки
            if q==8: # Расчет по Радиусу и длине Дуги
            # Далее Cдвиг – четыре пробела в начале каждой строки
                R=0.00000000
                R, done1 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите Радиус: ')
                R=float(R) # Радиус – Принудительно в вещественное число
                L=0.00000000
                R, done2 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите длину Дуги: ')
                L=float(L) # Хорда – Принудительно в вещественное число
                ygr=(2*Pii)* (L/((R+R)*Pii)) # Угол раствора хорды радиан
                x=math.sin(ygr/2)
                y=R*x
                a=y+y # Хорда
                t=((R*R)-(y*y))
                z=math.sqrt(t) # Квадратный корень из " y "
                b=R-z # Прогиб
                yg=ygr*180/Pii # Угол раствора хорды град.
            # ......... ........ ......... ......... ......... ............
            # Далее Конец Cдвига – четыре пробела в начале каждой строки
            # Расчет по Прогибу и длине Дуги "
            if q==9: # Расчет по Радиусу и длине Дуги
            # Далее Cдвиг – четыре пробела в начале каждой строки
                b=0.00000000
                b, done1 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите стрелу Прогиба: ')
                b=float(b) # Радиус – Принудительно в вещественное число
                L=0.00000000
                L, done2 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите длину Дуги: ')
                L=float(L) # Хорда – Принудительно в вещественное число
                bb=0.000000 # Текущий прогиб
                Rt=L/Pii # Начальный Текущий радиус
                xx=b/100000 # Приращение радиуса
                xb=b/1000 # Допустимая ошибка..
                db=10000000*b # Начальное значение ошибки
                ugr=0.0000000
                # Уточнение подбором
                while db >xb:
            # Далее Cдвиг – восемь пробелов в начале каждой строки
                    Rt=Rt+xx
                    ygr=(2*Pii)* (L/((Rt+Rt)*Pii)) # Угол раствора хорды радиан
                    x=math.sin(ygr/2)
                    y=Rt*x
                    a=y+y # Хорда
                    t=((Rt*Rt)-(y*y))
                    z=math.sqrt(t) # Квадратный корень из " y "
                    bb=Rt-z # Прогиб
                    x=(b-bb)*(b-bb)
                    db=math.sqrt(x) # Квадратный корень из " x "
            # Далее Cдвиг – четыре пробела в начале каждой строки
                db=db+xb
                xx=b/100000000 # Приращение радиуса
                xb=b/1000000
                # Уточнение подбором ( вторая ступень )
                while db >xb:
            # Далее Cдвига – восемь пробелов в начале каждой строки
                    Rt=Rt+xx
                    ygr=(2*Pii)* (L/((Rt+Rt)*Pii)) # Угол раствора хорды радиан
                    x=math.sin(ygr/2)
                    y=Rt*x
                    a=y+y # Хорда
                    t=((Rt*Rt)-(y*y))
                    z=math.sqrt(t) # Квадратный корень из " y "
                    bb=Rt-z # Прогиб
                    x=(b-bb)*(b-bb)
                    db=math.sqrt(x) # Квадратный корень из " x "
            # Далее Cдвига – четыре пробела в начале каждой строки
            # Конец подбора ........ ....... ....... ....... ....... ....... ........
                R=Rt
                yg=ygr*180/Pii # Угол раствора хорды град.
            # Далее Конец Cдвига – четыре пробела в начале каждой строки
            # ,,,,,,,,, ,,,,,,,,,, ,,,,,,,, ,,,,,,,, ,,,,,,,,, ,,,,,,,,
            # ......... ........ ......... ......... ......... ............
            # Расчет по Углу раствора и Прогибу "
            if q==10: # Расчет по Прогибу и Углу раствора
            # Далее Cдвиг – четыре пробела в начале каждой строки
                yg=0.00000000
                yg, done1 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите Угол раствора в градусах: ')
                yg=float(yg) # Угол – Принудительно в вещественное число
                yr=yg*Pii/180
                b=0.00000000
                b, done2 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите стрелу Прогиба: ')
                b=float(b) # Радиус – Принудительно в вещественное число
                z=math.cos(yr/2)
                R=b/(1-z)
                x=0.00000000
                y=0.00000000
                v=0.00000000
                Sk=0.0000000
                St=0.00000000
                S=0.000000000
                Ex=1.000000
                x= math.cos(yr/2)
                y=R*x
                x=(R*R)-(y*y)
                c=math.sqrt(x) # Квадратный корень из " x "
                a=c+c
                au=yg
                D=R+R
            # ......... ........ ......... ......... ......... ............
            # Далее Конец Cдвига – четыре пробела в начале каждой строки
            # Расчет по Углу раствора и длине Дуги "
            if q==11: # Расчет по Углу раствора и длине Дуги
            # Далее Cдвиг – четыре пробела в начале каждой строки
                yg=0.00000000
                yg, done1 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите Угол раствора в градусах: ')
                yg=float(yg) # Радиус – Принудительно в вещественное число
                L=0.00000000
                L, done2 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите длину Дуги: ')
                L=float(L) # Хорда – Принудительно в вещественное число
                C=L*360/yg
                D=C/Pii
                R=D/2
                x=0.00000000
                y=0.00000000
                v=0.00000000
                Sk=0.0000000
                St=0.00000000
                S=0.000000000
                Ex=1.000000
                yr=yg*Pii/180
                x= math.cos(yr/2)
                y=R*x
                b=R-y
                z=(R*R)-(y*y)
                c=math.sqrt(z) # Квадратный корень из " x "
                a=c+c
            # .......... ........ ....... ....... ........ ..........
            # Далее Конец Cдвига – четыре пробела в начале каждой строки
            # ......... ........ ......... ......... ......... ............
            if q>3: # Вывод по Хорде Прогиб Радиус
            # Далее Cдвиг – четыре пробела в начале каждой строки
                if q<12: # Вывод по Хорде Прогиб Радиус
            # Далее Cдвиг – восемь пробелов в начале каждой строки
                    D=R+R
                    L=D*Pii*yg/360
                    Sk=Pii*D*D*yg/(4*360) # Площадь сектора круга с углом aur
                    St=a*(R-b)/2 # Площадь треугольника в секторе
                    S = Sk-St # Площадь горбушки
                    self.result_label.setText(f'Радиус = {str(R)}\nДиаметр = {str(D)}\nСтрела прогиба = {str(b)}\nХорда = {str(a)} \nУгол раствора хорды = {str(yg)}\nДлина дуги над хордой = {str(L)}\nПлощадь сектора под дугой = {str(Sk)} \nПлощадь треугольника под горбушкой = {str(St)} \n Площадь горбушки = {str(S)}')

            # Далее Конец Cдвига – восемь пробелов в начале каждой строки
            # ,,,,,, ,,,,,, ,,,,,, ,,,,,, ,,,,,, ,,,,,,, ,,,,,,,,,
            if q==12: # Расчет Правильного многогранника
            # Далее Cдвиг – четыре пробела в начале каждой строки
                D=0.00000000
                D, done1 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите Описанный диаметр (при вводе нуля – переход на вписанный диаметр): ')
                D=float(D) # – Принудительно в вещественное число
                x=0.00000000
                y=0.00000000
                v=0.00000000
                Sk=0.0000000
                St=0.00000000
                S=0.000000000
                Ex=10000000
                vv=0 # Флаг расчета при заданном Описанном диаметре
                vv=float(vv)
                if D==0:
            # Далее Cдвиг – восемь пробелов в начале каждой строки
                    Sv=0.00000000
                    Sv, done2 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите Вписанный диаметр: ')
                    Sv=float(Sv) # – Принудительно в вещественное число
                    n=0.00000000
                    n, done2 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите число Граней: ')
                    n=float(n) # Принудительно в вещественное число
                    sur=2*Pii/n # Угол А в радианах
                    su=360/n # Угол А в градусах
                    au=su
                    yg=au
                    yr=sur
                    x=math.cos(sur/2) # Cos Угла А
                    y=Sv/2
                    R=y/x
                    D=R+R
                    vv=1 # Флаг расчета при заданном Вписанном диаметре
            # Далее Cдвиг – четыре пробела в начале каждой строки
                if vv==0:
            # Далее Cдвиг – восемь пробелов в начале каждой строки
                    n=0.00000000
                    n, done2 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите число Граней: ')
                    n=float(n) # Принудительно в вещественное число
                    R=D/2
                    sur=2*Pii/n # Угол А в радианах
                    su=360/n # Угол А в градусах
                    au=su
                    yg=au
                    yr=sur
                    x=math.cos(sur/2) # Cos Угла А
                    y=R*x
            # Далее Cдвиг – четыре пробела в начале каждой строки
                Sh=y
                Sv=y+y
                b=R-y
                x=(R*R)-(y*y)
                c=math.sqrt(x) # Квадратный корень из " x "
                a=c+c
                Sm=(a*(R-b)/2)*n # Площадь многогранника
                nn=0
                # Вывод по Многограннику
                self.result_label.setText(f'Описанный диаметр = {str(D)}\nЧисло граней = {str(n)} \nВысота: Грань – Центр = {str(Sh)} \nВписанный диаметр = {str(Sv)}\nШирина грани = {str(a)} \nПлощадь Многогранника = {str(Sm)}')
            # Далее Конец Cдвига – четыре пробела в начале каждой строки
            # ...... ....... ....... ....... ....... ........ .......
            if q==13: # Координаты радиусной прямой
            # Далее Cдвиг – четыре пробела в начале каждой строки
            # ....... ....... ....... ...... ...... ....... ....... ......
                result = ''
                a=0.00000000
                y=0.00000000
                ug=0.00000000
                ugg=0.00000000
                R=0.00000000
                hm=0.00000000
                ht= 0.0000000
                R, done1 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите радиус: ')
                R=float(R) # Принудительно в вещественное число
                u=" Радиус заданной кривой = "
                ss=str(R) # Преобразуем число в строку
                Rad=u+ss
                u=Rad+"\n"
                result += u
                u=" Введите Хорду "
                a, done2 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите Хорду: ')
                a=float(a) # Принудительно в вещественное число
                u=" Хорда максимальная заданная = "
                ss=str(a) # Преобразуем число в строку
                Xord=u+ss
                u=Xord+"\n"
                result += u
                # Находим значения максимального прогиба при Lx = 0....
                x=((R*R)-(a*a/4))
                b=R-(math.sqrt(x)) # Квадратный корень из " x "
                # b – максимальный прогиб…
                u=" Подъем максимальный в центре хорды = "
                ss=str(b) # Преобразуем число в строку
                Progi=u+ss
                u=Progi+"\n"
                result += u
                x=(a/2)/R
                y=math.asin(x)
                v=math.cos(y)
                aur=2*y
                au=aur*180/Pii # Угол А в градусах
                Lx= 0.0000001 # <Начальное значение>
                while Lx>0:
            # Далее Cдвиг – восемь пробелов в начале каждой строки
                    Lx, done2 = QtWidgets.QInputDialog.getText(MainWindow, 'Ввод начальных данных', 'Введите растояние от центра хорды до перпендикуляра: ')
                    if not done2: break
                    Lx=float(Lx)
                    u=" От центра хорды до точки по оси Х‑Х = "
                    ss=str(Lx) # Преобразуем число в строку
                    Xord=u+ss
                    u=Xord+"\n"
                    result += u
                    x=(R*R)-(Lx*Lx)
                    z=math.sqrt(x)
                    y=R-z # Прогиб при хорде = Lx*2
                    ht=b-y # Расчитали величину подьема
                    u=" На растоянии от центра = "
                    ss=str(Lx) # Преобразуем число в строку
                    u=u+ss
                    result += u
                    u=" Величина подьема ( перпендикуляра ) = "
                    ss=str(ht) # Преобразуем число в строку
                    u=u+ss
                    result += u
                    self.result_label.setText(result)
        except ValueError:
            self.result_label.setText('Произошла ошибка во время подсчётов. Проверьте исходные данные.')

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

import math # Подключили математич модуль
# Отступ – четыре пробела..
# Все проверено – 23-06-2014 г..
global Rad # Радиус – Обьявили глобальную переменную
global Diam # Диаметр
global Xord # Хорда
global Progi # Прогиб
global UgSe # Угол раствора радиусов на хорду
global Dug # Длина дуги над хордой
global PlSe # Площадь сектора на хорду
global PlTr # Площадь Треугольника под хордой
global PlGo # Площадь Горбушки
uu =" "
u=" Расчет элементов Хорда – Прогиб – Радиус "
print (uu)
print (u)
print (uu)
u=" ...... ...... ...... ...... ...... ...... ..... ...... "
print (u)
Pii=0.000000
Pii=math.pi # Вытащили число " Пи "
Kvz=0 # Флаг – Если =1 то работа уже выполнена.
print (uu)
u=" 1 – Расчет по Хорде – Радиусу "
print (u)
print (uu)
u=" 2 – Расчет по Хорде и Углу раствора "
print (u)
print (uu)
u=" 3 – Расчет координат Радиусной кривой "
print (u)
print (uu)
u=" 4 – Расчет по Хорде и длине Дуги "
print (u)
print (uu)
u=" 5 – Расчет по Хорде и Прогибу "
print (u)
print (uu)
u=" 6 – Расчет по Радиусу и Углу раствора "
print (u)
print (uu)
u=" 7 – Расчет по Радиусу и Прогибу "
print (u)
print (uu)
u=" 8 – Расчет по Радиусу и длине Дуги "
print (u)
print (uu)
u=" 9 – Расчет по Прогибу и длине Дуги "
print (u)
print (uu)
u=" 10 – Расчет по Прогибу и Углу раствора "
print (u)
print (uu)
u=" 11 – Расчет по Углу раствора и длине Дуги "
print (u)
print (uu)
u=" 12 – Расчет Правильного многогранника "
print (u)
print (uu)
u=" ...... ...... ...... ...... ...... ...... ..... ...... "
print (u)
q=0
q=input( ) # Вводим число
q=float(q) # Принудительно в вещественное число
# ...... ....... ....... ....... ....... ........ .......
if q==1: # Расчет по Хорде – Радиусу
    # Далее Cдвиг – четыре пробела в начале каждой строки
    print (uu)
    print (uu)
    u=" Расчет по Хорде – Радиусу "
    print (u)
    print (uu)
    u=" Введите Хорду "
    print (u)
    print (uu)
    a=0.00000000
    a=input( ) # Вводим число
    a=float(a) # Принудительно в вещественное число
    u=" Введите Радиус "
    print (u)
    print (uu)
    R=0.00000000
    R=input( ) # Вводим число
    R=float(R) # Принудительно в вещественное число
    x=0.00000000
    y=0.00000000
    v=0.00000000
    Sk=0.0000000
    St=0.00000000
    S=0.000000000
    x=((R*R)-(a*a/4))
    b=R-(math.sqrt(x)) # Квадратный корень из " x "
    x=(a/2)/R
    y=math.asin(x)
    v=math.cos(y)
    aur=2*y
    au=aur*180/Pii # Угол А в градусах
    Kvz=1 # Коэф – Расчет уже выполнен
# Далее Конец Cдвига – четыре пробела в начале каждой строки
else:
# Далее Cдвиг – четыре пробела в начале каждой строки
    Kvz=0
# ...... ....... ....... ....... ....... ........ .......
# Далее Конец Cдвига – четыре пробела в начале каждой строки
if q==2: # Расчет по Хорде и углу раствора
    # Далее Cдвиг – четыре пробела в начале каждой строки
    print (uu)
    print (uu)
    u=" Расчет по Хорде и углу раствора "
    print (u)
    print (uu)
    u=" Введите Хорду "
    print (u)
    print (uu)
    a=0.00000000
    a=input( ) # Вводим число
    a=float(a) # Принудительно в вещественное число
    u=" Введите Угол раствора в градусах "
    print (u)
    print (uu)
    au=0.00000000
    au=input( ) # Вводим число
    au=float(au) # Принудительно в вещественное число
    x=0.00000000
    aur=0.00000000
    v=0.00000000
    y=0.00000000
    Sk=0.0000000
    St=0.00000000
    S=0.000000000
    aur=au*Pii/180
    x=math.sin(aur/2)
    R=(a/2)/x # Нашли радиус
    y=(R*R)-(a*a/4)
    v=math.sqrt(y) # Квадратный корень из " y "
    b=R-v # Нашли прогиб
    Kvz=1 # Коэф – Расчет уже выполнен
# Далее Конец Cдвига – четыре пробела в начале каждой строки
else:
# Далее Cдвиг – четыре пробела в начале каждой строки
    Kvz=0
# ...... ....... ....... ....... ....... ........ .......
# Далее Конец Cдвига – четыре пробела в начале каждой строки
if q<3: # Вывод по Хорде Прогиб Радиус
    # Далее Cдвиг – четыре пробела в начале каждой строки
    D=R+R
    Sk=Pii*D*D*au/(4*360) # Площадь сектора круга с углом aur
    St=(a/2)*(R-b) # Площадь треугольника в секторе
    S = Sk-St # Площадь горбушки
    L=Pii*D*au/360 # Длина дуги
    print (uu)
    u=" ,,,, ,,,, ,,,, ,,,,, ,,,,, ,,,, ,,,,, ,,,,, ,,,,, "
    print (u)
    print (uu)
    print (uu)
    u=" Хорда = "
    ss=str(a) # Преобразуем число в строку
    u=u+ss
    print (u)
    Xord=u
    print (uu)
    u=" Стрела прогиба = "
    ss=str(b) # Преобразуем число в строку
    u=u+ss
    print (u)
    Progi=u
    print (uu)
    u=" Радиус = "
    ss=str(R) # Преобразуем число в строку
    u=u+ss
    print (u)
    Rad=u
    print (uu)
    u=" Диаметр = "
    ss=str(D) # Преобразуем число в строку
    u=u+ss
    print (u)
    Diam=u
    print (uu)
    u=" Угол раствора хорды = "
    ss=str(au) # Преобразуем число в строку
    u=u+ss
    print (u)
    UgSe=u
    print (uu)
    u=" Длина дуги над хордой = "
    ss=str(L) # Преобразуем число в строку
    u=u+ss
    print (u)
    Dug=u
    print (uu)
    u=" Площадь сектора = "
    ss=str(Sk) # Преобразуем число в строку
    u=u+ss
    print (u)
    PlSe=u
    print (uu)
    u=" Площадь треугольника под горбушкой = "
    ss=str(St) # Преобразуем число в строку
    u=u+ss
    print (u)
    PlTr=u
    print (uu)
    u=" Площадь горбушки = "
    ss=str(S) # Преобразуем число в строку
    u=u+ss
    print (u)
    PlGo=u
    print (uu)
# Далее Конец Cдвига – четыре пробела в начале каждой строки
# ...... ....... ....... ....... ....... ........ .......
if q==3: # Координаты радиусной прямой
# Далее Cдвиг – четыре пробела в начале каждой строки
    q=13 # Переброс в конец программы
# ....... ....... ....... ...... ...... ....... ....... ......
# Далее Конец Cдвига – четыре пробела в начале каждой строки
if q==4: # Расчет по Хорде и длине Дуги
    # Далее Cдвиг – четыре пробела в начале каждой строки
    print (uu)
    print (uu)
    u=" Расчет по Хорде и и длине Дуги "
    print (u)
    print (uu)
    u=" Введите Хорду "
    print (u)
    print (uu)
    a=0.00000000
    a=input( ) # Вводим число
    a=float(a) # Принудительно в вещественное число
    u=" Введите длину Дуги "
    print (u)
    print (uu)
    L=0.00000000
    L=input( ) # Вводим число
    L=float(L) # Принудительно в вещественное число
    R=0.00000000
    dx=a/200000
    rt=(a/2)+dx # Начальный радиус расчета
    aa=a/2 # Половина хорды
    dl=Pii*rt
    while dl>L: # Расчет по Хорде и длине Дуги
        # Далее Cдвиг – восемь пробелов в начале каждой строки
        rt=rt+dx # Текущий радиус
        x=aa/rt
        y=2*(math.asin(x)) # Угол А в радианах через арс синус
        dl=rt*y # Текущая длина дуги
    # Далее Cдвиг – четыре пробела в начале каждой строки
    yg=y*180/Pii # Угол " y " в градусах
    au=yg
    R=rt # Нашли радиус
    y=(R*R)-(aa*aa)
    v=math.sqrt(y) # Квадратный корень из " y "
    b=R-v # Нашли прогиб
    D=R+R
    Sk=Pii*D*D*yg/(4*360) # Площадь сектора круга с углом aur
    St=aa*v # Площадь треугольника в секторе
    S = Sk-St # Площадь горбушки
# Далее Конец Cдвига – четыре пробела в начале каждой строки
else:
# Далее Cдвиг – четыре пробела в начале каждой строки
    Kvz=0
# Далее Конец Cдвига – четыре пробела в начале каждой строки
# ...... ...... ....... ...... ...... ....... ....... ....... ........ ........ ........ .......
if q==5: # Расчет по Хорде – Прогибу
    # Далее Cдвиг – четыре пробела в начале каждой строки
    print (uu)
    u=" Расчет по Хорде – Прогибу "
    print (u)
    print (uu)
    u=" ...... ...... ...... ...... ...... ...... ..... ...... "
    print (u)
    print (uu)
    u=" Введите Хорду "
    print (u)
    print (uu)
    a=0.00000000
    a=input( ) # Вводим число
    a=float(a) # Хорда – Принудительно в вещественное число
    u=" Введите Стрелу Прогиба "
    print (uu)
    print (u)
    print (uu)
    b=0.00000000
    b=input( ) # Вводим число
    b=float(b) # Стрела прогиба – Принудительно в вещественное число
    x=0.00000000
    y=0.00000000
    v=0.00000000
    R=0.00000000
    Sk=0.0000000
    St=0.00000000
    S=0.000000000
    Ex=1.000000
    k=a/2
    Rt=k+(k/100000)
    Rd= k/100000
    t=k/100000000
    # Уточнение подбором
    while Ex >t:
        # Далее Cдвиг – восемь пробелов в начале каждой строки
        Rt=Rt+Rd
        x=((Rt*Rt)-(k*k))
        c=math.sqrt(x) # Квадратный корень из " x "
        Ex=(Rt-c)-b
# E=math.abs(x)
# Конец подбора ........ ....... ......
    # Далее Cдвиг – четыре пробела в начале каждой строки
    R=Rt
    D=R+R
    x=k/Rt
    sur=math.asin(x) # Угол А в радианах
    su=sur*180/Pii # Угол А в градусах
    au=2*su
    yg=au
    L=(R+R)*Pii*au/360
# ....... ....... ....... ...... ...... ....... ....... ....... ........
# Далее Конец Cдвига – четыре пробела в начале каждой строки
if q==6: # Расчет по Радиусу и Углу раствора
    # Далее Cдвиг – четыре пробела в начале каждой строки
    print (uu)
    u=" Расчет по Радиусу и Углу раствора "
    print (u)
    print (uu)
    u=" ...... ...... ...... ...... ...... ...... ..... ...... "
    print (u)
    print (uu)
    u=" Введите Радиус "
    print (u)
    print (uu)
    R=0.00000000
    R=input( ) # Вводим число
    R=float(R) # Радиус – Принудительно в вещественное число
    u=" Введите Угол раствора в градусах "
    print (uu)
    print (u)
    print (uu)
    yg=0.00000000
    yg=input( ) # Вводим число
    yg=float(yg) # Угол раствора – Принудительно в вещественное число
    yr=yg*Pii/180 # Угол раствора В радианах
    x=0.00000000
    y=0.00000000
    v=0.00000000
    Sk=0.0000000
    St=0.00000000
    S=0.000000000
    Ex=1.000000
    x= math.cos(yr/2)
    y=R*x
    b=R-y
    x=(R*R)-(y*y)
    c=math.sqrt(x) # Квадратный корень из " x "
    a=c+c
    au=yg
    D=R+R
# .......... ........ ....... ....... ........ ..........
# Далее Конец Cдвига – четыре пробела в начале каждой строки
# ....... ....... ....... ...... ...... ....... ....... ....... .
if q==6: # Расчет по Радиусу и Углу раствора
# Далее Cдвиг – четыре пробела в начале каждой строки
    print (uu)
    u=" Расчет по Радиусу и Углу раствора "
    print (u)
    print (uu)
    u=" ...... ...... ...... ...... ...... ...... ..... ...... "
    print (u)
    print (uu)
    u=" Введите Радиус "
    print (u)
    print (uu)
    R=0.00000000
    R=input( ) # Вводим число
    R=float(R) # Радиус – Принудительно в вещественное число
    u=" Введите Угол раствора в градусах "
    print (uu)
    print (u)
    print (uu)
    yg=0.00000000
    yg=input( ) # Вводим число
    yg=float(yg) # Угол раствора – Принудительно в вещественное число
    yr=yg*Pii/180 # Угол раствора В радианах
    x=0.00000000
    y=0.00000000
    v=0.00000000
    Sk=0.0000000
    St=0.00000000
    S=0.000000000
    Ex=1.000000
    x= math.cos(yr/2)
    y=R*x
    b=R-y
    x=(R*R)-(y*y)
    c=math.sqrt(x) # Квадратный корень из " x "
    a=c+c
    au=yg
    D=R+R
# .......... ........ ....... ....... ........ ..........
# Далее Конец Cдвига – четыре пробела в начале каждой строки
# ....... ....... ....... ...... ...... ....... ....... ....... .
if q==7: # Расчет по Радиусу и Прогибу
# Далее Cдвиг – четыре пробела в начале каждой строки
    print (uu)
    u=" Расчет по Радиусу и Прогибу "
    print (u)
    print (uu)
    u=" ...... ...... ...... ...... ...... ...... ..... ...... "
    print (u)
    print (uu)
    u=" Введите Радиус "
    print (u)
    print (uu)
    R=0.00000000
    R=input( ) # Вводим число
    R=float(R) # Радиус – Принудительно в вещественное число
    u=" Введите Прогиб "
    print (uu)
    print (u)
    print (uu)
    b=0.00000000
    b=input( ) # Вводим число
    b=float(b) # Угол раствора – Принудительно в вещественное число
    k=R-b
    x=(R*R)-(k*k)
    a=(math.sqrt(x))*2
    x=(a/2)/k
    aur = (math.atan(x))*2
    au=aur*180/Pii
    yg = au
# ....... ....... ....... ...... ...... ....... ....... ....... ........
# Далее Конец Cдвига – четыре пробела в начале каждой строки
if q==8: # Расчет по Радиусу и длине Дуги
# Далее Cдвиг – четыре пробела в начале каждой строки
    print (uu) 
    u=" Расчет по Радиусу и длине Дуги "
    print (u)
    print (uu)
    u=" ...... ...... ...... ...... ...... ...... ..... ...... "
    print (u)
    print (uu)
    u=" Введите Радиус "
    print (u)
    print (uu)
    R=0.00000000
    R=input( ) # Вводим число
    R=float(R) # Радиус – Принудительно в вещественное число

    print (uu)
    u=" Введите длину Дуги "
    print (u)
    print (uu)
    L=0.00000000
    L=input( ) # Вводим число
    L=float(L) # Хорда – Принудительно в вещественное число
    ygr=(2*Pii)* (L/((R+R)*Pii)) # Угол раствора хорды радиан
    x=math.sin(ygr/2)
    y=R*x
    a=y+y # Хорда
    t=((R*R)-(y*y))
    z=math.sqrt(t) # Квадратный корень из " y "
    b=R-z # Прогиб
    yg=ygr*180/Pii # Угол раствора хорды град.
# ......... ........ ......... ......... ......... ............
# Далее Конец Cдвига – четыре пробела в начале каждой строки
# Расчет по Прогибу и длине Дуги "
if q==9: # Расчет по Радиусу и длине Дуги
# Далее Cдвиг – четыре пробела в начале каждой строки
    print (uu)
    u=" Расчет по Прогибу и длине Дуги "
    print (u)
    print (uu)
    u=" ...... ...... ...... ...... ...... ...... ..... ...... "
    print (u)
    print (uu)
    u=" Введите стрелу Прогиба "
    print (u)
    print (uu)
    b=0.00000000
    b=input( ) # Вводим число
    b=float(b) # Радиус – Принудительно в вещественное число
    print (uu)
    u=" Введите длину Дуги "
    print (u)
    print (uu)
    L=0.00000000
    L=input( ) # Вводим число
    L=float(L) # Хорда – Принудительно в вещественное число
    print (uu)
    u=" Подождите – идет расчет "
    print (u)
    print (uu)
    bb=0.000000 # Текущий прогиб
    Rt=L/Pii # Начальный Текущий радиус
    xx=b/100000 # Приращение радиуса
    xb=b/1000 # Допустимая ошибка..
    db=10000000*b # Начальное значение ошибки
    ugr=0.0000000
    # Уточнение подбором
    while db >xb:
# Далее Cдвиг – восемь пробелов в начале каждой строки
        Rt=Rt+xx
        ygr=(2*Pii)* (L/((Rt+Rt)*Pii)) # Угол раствора хорды радиан
        x=math.sin(ygr/2)
        y=Rt*x
        a=y+y # Хорда
        t=((Rt*Rt)-(y*y))
        z=math.sqrt(t) # Квадратный корень из " y "
        bb=Rt-z # Прогиб
        x=(b-bb)*(b-bb)
        db=math.sqrt(x) # Квадратный корень из " x "
# Далее Cдвиг – четыре пробела в начале каждой строки
    db=db+xb
    xx=b/100000000 # Приращение радиуса
    xb=b/1000000
    # Уточнение подбором ( вторая ступень )
    while db >xb:
# Далее Cдвига – восемь пробелов в начале каждой строки
        Rt=Rt+xx
        ygr=(2*Pii)* (L/((Rt+Rt)*Pii)) # Угол раствора хорды радиан
        x=math.sin(ygr/2)
        y=Rt*x
        a=y+y # Хорда
        t=((Rt*Rt)-(y*y))
        z=math.sqrt(t) # Квадратный корень из " y "
        bb=Rt-z # Прогиб
        x=(b-bb)*(b-bb)
        db=math.sqrt(x) # Квадратный корень из " x "
# Далее Cдвига – четыре пробела в начале каждой строки
# Конец подбора ........ ....... ....... ....... ....... ....... ........
    R=Rt
    yg=ygr*180/Pii # Угол раствора хорды град.
# Далее Конец Cдвига – четыре пробела в начале каждой строки
# ,,,,,,,,, ,,,,,,,,,, ,,,,,,,, ,,,,,,,, ,,,,,,,,, ,,,,,,,,
# ......... ........ ......... ......... ......... ............
# Расчет по Углу раствора и Прогибу "
if q==10: # Расчет по Прогибу и Углу раствора
# Далее Cдвиг – четыре пробела в начале каждой строки
    print (uu)
    u=" Расчет по Прогибу и Углу раствора "
    print (u)
    print (uu)
    u=" ...... ...... ...... ...... ...... ...... ..... ...... "
    print (u)
    print (uu)
    u=" Введите Угол раствора в градусах "
    print (u)
    print (uu)
    yg=0.00000000
    yg=input( ) # Вводим число
    yg=float(yg) # Угол – Принудительно в вещественное число
    yr=yg*Pii/180
    u=" Введите стрелу Прогиба "
    print (u)
    print (uu)
    b=0.00000000
    b=input( ) # Вводим число
    b=float(b) # Радиус – Принудительно в вещественное число
    z=math.cos(yr/2)
    R=b/(1-z)
    x=0.00000000
    y=0.00000000
    v=0.00000000
    Sk=0.0000000
    St=0.00000000
    S=0.000000000
    Ex=1.000000
    x= math.cos(yr/2)
    y=R*x
    x=(R*R)-(y*y)
    c=math.sqrt(x) # Квадратный корень из " x "
    a=c+c
    au=yg
    D=R+R
# ......... ........ ......... ......... ......... ............
# Далее Конец Cдвига – четыре пробела в начале каждой строки
# Расчет по Углу раствора и длине Дуги "
if q==11: # Расчет по Углу раствора и длине Дуги
# Далее Cдвиг – четыре пробела в начале каждой строки
    print (uu)
    u=" Расчет по Углу раствора и длине Дуги "
    print (u)
    print (uu)
    u=" ...... ...... ...... ...... ...... ...... ..... ...... "
    print (u)
    print (uu)
    u=" Введите Угол раствора в градусах "
    print (u)
    print (uu)
    yg=0.00000000
    yg=input( ) # Вводим число
    yg=float(yg) # Радиус – Принудительно в вещественное число
    print (uu)
    u=" Введите длину Дуги "
    print (u)
    print (uu)
    L=0.00000000
    L=input( ) # Вводим число
    L=float(L) # Хорда – Принудительно в вещественное число
    C=L*360/yg
    D=C/Pii
    R=D/2
    x=0.00000000
    y=0.00000000
    v=0.00000000
    Sk=0.0000000
    St=0.00000000
    S=0.000000000
    Ex=1.000000
    yr=yg*Pii/180
    x= math.cos(yr/2)
    y=R*x
    b=R-y
    z=(R*R)-(y*y)
    c=math.sqrt(z) # Квадратный корень из " x "
    a=c+c
# .......... ........ ....... ....... ........ ..........
# Далее Конец Cдвига – четыре пробела в начале каждой строки
# ......... ........ ......... ......... ......... ............
if q>3: # Вывод по Хорде Прогиб Радиус
# Далее Cдвиг – четыре пробела в начале каждой строки
    if q<12: # Вывод по Хорде Прогиб Радиус
# Далее Cдвиг – восемь пробелов в начале каждой строки
        D=R+R
        L=D*Pii*yg/360
        Sk=Pii*D*D*yg/(4*360) # Площадь сектора круга с углом aur
        St=a*(R-b)/2 # Площадь треугольника в секторе
        S = Sk-St # Площадь горбушки
        print (uu)
        u=" ,,,, ,,,, ,,,, ,,,,, ,,,,, ,,,, ,,,,, ,,,,, ,,,,, "
        print (u)
        print (uu)
        print (uu)
        u=" Радиус = "
        ss=str(R) # Преобразуем число в строку
        u=u+ss
        print (u)
        Rad=u
        print (uu)
        u=" Диаметр = "
        ss=str(D) # Преобразуем число в строку
        u=u+ss
        print (u)
        Diam=u
        print (uu)
        u=" Стрела прогиба = "
        ss=str(b) # Преобразуем число в строку
        u=u+ss
        print (u)
        Progi=u
        print (uu)
        u=" Хорда = "
        ss=str(a) # Преобразуем число в строку
        u=u+ss
        print (u)
        Xord=u
        print (uu)
        u=" Угол раствора хорды = "
        ss=str(yg) # Преобразуем число в строку
        u=u+ss
        print (u)
        UgSe=u
        print (uu)
        u=" Длина дуги над хордой = "
        ss=str(L) # Преобразуем число в строку
        u=u+ss
        print (u)
        Dug=u
        print (uu)
        u=" Площадь сектора под дугой = "
        ss=str(Sk) # Преобразуем число в строку
        u=u+ss
        print (u)
        PlSe=u
        print (uu)
        u=" Площадь треугольника под горбушкой = "
        ss=str(St) # Преобразуем число в строку
        u=u+ss
        print (u)
        PlTr=u
        print (uu)
        u=" Площадь горбушки = "
        ss=str(S) # Преобразуем число в строку
        u=u+ss
        print (u)
        PlGo=u
        print (uu)
# Далее Конец Cдвига – восемь пробелов в начале каждой строки
# ,,,,,, ,,,,,, ,,,,,, ,,,,,, ,,,,,, ,,,,,,, ,,,,,,,,,
if q==12: # Расчет Правильного многогранника
# Далее Cдвиг – четыре пробела в начале каждой строки
    print (uu)
    u=" Расчет по Правильного многогранника "
    print (u)
    print (uu)
    u=" ...... ...... ...... ...... ...... ...... ..... ...... "
    print (u)
    print (uu)
    u=" Введите Описанный диаметр "
    print (u)
    u=" при вводе нуля – переход на вписанный диаметр "
    print (u)
    print (uu)
    D=0.00000000
    D=input( ) # Вводим число
    D=float(D) # – Принудительно в вещественное число
    x=0.00000000
    y=0.00000000
    v=0.00000000
    Sk=0.0000000
    St=0.00000000
    S=0.000000000
    Ex=10000000
    vv=0 # Флаг расчета при заданном Описанном диаметре
    vv=float(vv)
    if D==0:
# Далее Cдвиг – восемь пробелов в начале каждой строки
        u=" ...... ...... ...... ...... ...... ...... ..... ...... "
        print (u)
        print (uu)
        u=" Введите Вписанный диаметр "
        print (u)
        print (uu)
        Sv=0.00000000
        Sv=input( ) # Вводим число
        Sv=float(Sv) # – Принудительно в вещественное число
        u=" Введите число Граней "
        print (uu)
        print (u)
        print (uu)
        n=0.00000000
        n=input( ) # Вводим число
        n=float(n) # Принудительно в вещественное число
        sur=2*Pii/n # Угол А в радианах
        su=360/n # Угол А в градусах
        au=su
        yg=au
        yr=sur
        x=math.cos(sur/2) # Cos Угла А
        y=Sv/2
        R=y/x
        D=R+R
        vv=1 # Флаг расчета при заданном Вписанном диаметре
# Далее Cдвиг – четыре пробела в начале каждой строки
    if vv==0:
# Далее Cдвиг – восемь пробелов в начале каждой строки
        u=" Введите число Граней "
        print (uu)
        print (u)
        print (uu)
        n=0.00000000
        n=input( ) # Вводим число
        n=float(n) # Принудительно в вещественное число
        R=D/2
        sur=2*Pii/n # Угол А в радианах
        su=360/n # Угол А в градусах
        au=su
        yg=au
        yr=sur
        x=math.cos(sur/2) # Cos Угла А
        y=R*x
# Далее Cдвиг – четыре пробела в начале каждой строки
    Sh=y
    Sv=y+y
    b=R-y
    x=(R*R)-(y*y)
    c=math.sqrt(x) # Квадратный корень из " x "
    a=c+c
    Sm=(a*(R-b)/2)*n # Площадь многогранника
    nn=0
    # Вывод по Многограннику
    u=" Описанный диаметр = "
    ss=str(D) # Преобразуем число в строку
    u=u+ss
    print (u)
    print (uu)
    u=" Число граней = "
    ss=str(n) # Преобразуем число в строку
    u=u+ss
    print (u)
    print (uu)
    u=" Высота: Грань – Центр = "
    ss=str(Sh) # Преобразуем число в строку
    u=u+ss
    print (u)
    print (uu)
    u=" Вписанный диаметр = "
    ss=str(Sv) # Преобразуем число в строку
    u=u+ss
    print (u)
    print (uu)
    u=" Ширина грани = "
    ss=str(a) # Преобразуем число в строку7
    u=u+ss
    print (u)
    print (uu)
    u=" Площадь Многогранника = "
    ss=str(Sm) # Преобразуем число в строку
    u=u+ss
    print (u)
    print (uu)
# Далее Конец Cдвига – четыре пробела в начале каждой строки
# ...... ....... ....... ....... ....... ........ .......
if q==13: # Координаты радиусной прямой
# Далее Cдвиг – четыре пробела в начале каждой строки
# ....... ....... ....... ...... ...... ....... ....... ......
    f = open('Rezult.txt', 'w') # Открыли файл для записи
    # Записываем числа в текстовом виде
    u=uu+"\n" # Добавим код перевода строки
    f.write(u)
    u1=" Расчет координат точек на радиусной кривой "
    u=u1+"\n"
    f.write(u)
    uu=" "+"\n"
    f.write(uu)
    uuu=" ,,,,,,,,,, ,,,,,,,, ,,,,,,,, ,,,,,,,, ,,,,,,,, ,,,,,,,, "+"\n"
    f.write(uuu)
    f.write(uu)
    print (uu)
    u=" Построение большого радиуса методом подьема "
    print (u)
    print (uu)
    u=" Размер по горизонтали отсчитывается от центра хорды "
    print (u)
    print (uu)
    u=" от хорды проводим перпендикуляр расчитанной величины. "
    print (u)
    print (uu)
    u=" При вводе размера по горизонту = нулю – выход из программы "
    print (u)
    print (uu)
    print (uu)
    input( ) # Ожидание нажима Ентер
    print (uu)
    a=0.00000000
    y=0.00000000
    ug=0.00000000
    ugg=0.00000000
    R=0.00000000
    hm=0.00000000
    ht= 0.0000000
    print (uu)
    u=" Введите радиус "
    print (u)
    print (uu)
    R=input( ) # Вводим число
    R=float(R) # Принудительно в вещественное число
    u=" Радиус заданной кривой = "
    ss=str(R) # Преобразуем число в строку
    Rad=u+ss
    u=Rad+"\n"
    f.write(u)
    u=" Введите Хорду "
    print (u)
    print (uu)
    a=input( ) # Вводим число
    a=float(a) # Принудительно в вещественное число
    u=" Хорда максимальная заданная = "
    ss=str(a) # Преобразуем число в строку
    Xord=u+ss
    u=Xord+"\n"
    f.write(u)
    # Находим значения максимального прогиба при Lx = 0....
    x=((R*R)-(a*a/4))
    b=R-(math.sqrt(x)) # Квадратный корень из " x "
    # b – максимальный прогиб…
    u=" Подъем максимальный в центре хорды = "
    ss=str(b) # Преобразуем число в строку
    Progi=u+ss
    u=Progi+"\n"
    f.write(u)
    x=(a/2)/R
    y=math.asin(x)
    v=math.cos(y)
    aur=2*y
    au=aur*180/Pii # Угол А в градусах
    Lx= 0.0000001 # <Начальное значение>
    while Lx>0:
# Далее Cдвиг – восемь пробелов в начале каждой строки
        print (uu)
        u=" Введите растояние от центра хорды до перпендикуляра "
        print (u)
        print (uu)
        Lx=input( ) # Вводим число
        Lx=float(Lx)
        u=" От центра хорды до точки по оси Х‑Х = "
        ss=str(Lx) # Преобразуем число в строку
        Xord=u+ss
        u=Xord+"\n"
        f.write(uu)
        f.write(u)
        x=(R*R)-(Lx*Lx)
        z=math.sqrt(x)
        y=R-z # Прогиб при хорде = Lx*2
        ht=b-y # Расчитали величину подьема
        u=" На растоянии от центра = "
        ss=str(Lx) # Преобразуем число в строку
        u=u+ss
        print (u)
        print (uu)
        u=" Величина подьема ( перпендикуляра ) = "
        ss=str(ht) # Преобразуем число в строку
        u=u+ss
        print (u)
        f.write(u)
        f.write(uu)
        print (uu)
        u=" ================================================= "
        print (u)
        f.write(u)
        print (uu)
        input( ) # Ожидание нажима Ентер
# Далее Cдвиг – четыре пробела в начале каждой строки
    print (uu)
    input( ) # Ожидание нажима Ентер
    print (uu)
    # ....... ....... ....... ...... ...... ....... ....... ......
    f.write(uu)
    f.write(uuu)
    f.close() # закрыли файл
# Далее Конец Cдвига – четыре пробела в начале каждой строки
q=111 # обход всего, что дальше особенно записи в файл..
# ...... ....... ....... ....... ....... ........ .......
# ....... ....... ....... ...... ...... ....... ....... ....... ........ ........ ........
u=" ...... ...... ...... Конец программы ...... ...... ...... "
print (u)
print (uu)
input( ) # Ожидание нажима Ентер
# ..... ..... ..... Конец листинга программы ..... ....
